import dummyData from "../dummy-data";

const initialState = {
  data: [...dummyData]
};

const reducer = (state = initialState, action) => {
  if (
    action.type === "ADD_COMMENT" &&
    action.payload.event.key === "Enter" &&
    action.payload.event.target.value.trim() !== ""
  ) {
    const userArray = [
      "philzcoffee",
      "biancasaurus",
      "martinseludo",
      "twitch",
      "michaelmarzetta",
      "themexican_leprechaun",
      "dennis_futbol",
      "playhearthstone",
      "awaywetravel",
      "awesomebt28"
    ];
    const random = userArray[Math.floor(Math.random() * userArray.length)];
    const nextId = state.data[action.payload.userId - 1].comments.length + 1;
    const commentObj = {
      id: nextId,
      username: random,
      text: action.payload.event.target.value
    };
    const prevState = Object.assign({}, state);
    const newComments = prevState.data[
      action.payload.userId - 1
    ].comments.concat(commentObj);
    const newUser = { ...prevState.data[action.payload.userId - 1] };
    newUser.comments = newComments;
    action.payload.event.target.value = "";
    return {
      ...state,
      data: [
        ...state.data.slice(0, action.payload.userId - 1),
        newUser,
        ...state.data.slice(action.payload.userId)
      ]
    };
  }
  return state;
};

export default reducer;
