import React, { Component } from "react";
import "./PostContainer.css";
import CommentSection from "../CommentSection/CommentSection";
import Icon from "@material-ui/core/Icon";
import moment from "moment";

class PostContainer extends Component {
  render() {
    const newTime = this.props.user.timestamp.replace("th", "");
    return (
      <div className="wrap-post">
        <div className="user-post">
          <img src={this.props.user.thumbnailUrl} alt="userpic" />
          <span>{this.props.user.username}</span>
        </div>
        <div className="img-post">
          <img src={this.props.user.imageUrl} alt="userpost" />
        </div>
        <div className="icon-post">
          <div>
            <span>
              <Icon fontSize="large">favorite_border</Icon>
            </span>
            <span>
              <Icon fontSize="large">chat_bubble_outline</Icon>
            </span>
          </div>
          <p>
            {this.props.user.likes} Likes, &nbsp;{" "}
            {this.props.user.comments.length} Comments
          </p>
        </div>
        <div className="comment-post">
          <CommentSection
            comments={this.props.user.comments}
            idUser={this.props.user.id}
          />
        </div>
        <div className="time-post">
          <p>Posted {moment(new Date(newTime)).fromNow()}</p>
        </div>
      </div>
    );
  }
}

export default PostContainer;
