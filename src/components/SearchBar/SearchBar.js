import React from 'react';
import './SearchBar.css';
import Icon from '@material-ui/core/Icon';
//import { Button } from 'reactstrap';

const searchBar = props => {
    return (
        <div className='wrap-bar'>
            <div className='left'>
                <img src={require('../../assets/instagram.png')} alt='logo'/>
                <span className='vertical'></span>
                <img src={require('../../assets/igname.png')} alt='name'/>
            </div>
            <div className='middle'>
                <input type='text' placeholder='&#128270; Search...'></input>
            </div>
            <div className='right'>
                <span><Icon fontSize='large'>my_location</Icon></span>
                <span><Icon fontSize='large'>favorite_border</Icon></span>
                <span><Icon fontSize='large'>person_outl2ne</Icon></span>
                
            </div>
            
        </div>
    );
}

export default searchBar;
