import React from "react";
import { connect } from "react-redux";
import "./CommentSection.css";

const commentSection = props => {
  let comment = props.comments.map(com => {
    return (
      <p key={com.id}>
        <strong>{com.username}</strong>&nbsp; {com.text}
      </p>
    );
  });
  return (
    <div className="wrap-comments">
      {comment}
      <input
        type="text"
        placeholder="Add a comment..."
        onKeyDown={e => props.addComment(e, props.idUser)}
      />
    </div>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    addComment: (e, id) =>
      dispatch({ type: "ADD_COMMENT", payload: { event: e, userId: id } })
  };
};

export default connect(
  null,
  mapDispatchToProps
)(commentSection);
