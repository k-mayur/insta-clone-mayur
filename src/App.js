import React, { Component } from "react";
import { connect } from "react-redux";
import SearchBar from "./components/SearchBar/SearchBar";
import PostContainer from "./components/PostContainer/PostContainer";
import "./App.css";

class App extends Component {
  render() {
    const posts = this.props.data.map((post, i) => {
      return <PostContainer user={post} key={i} />;
    });

    return (
      <div className="App">
        <SearchBar />
        {posts}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.data
  };
};

export default connect(mapStateToProps)(App);
